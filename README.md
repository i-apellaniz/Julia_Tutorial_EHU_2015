# Invitation to Julia for scientific computing

This tutorial is based on the previous work of David P. Sanders. The intention 
of this workshop to see with practical examples the most awesome features
of the Julia language, http://www.julialang.org.